from django.conf.urls import url
from .views import index, show, create

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^(?P<id>\d+)/$', show, name='show'),
    url(r'^create/$', create, name='create'),
]