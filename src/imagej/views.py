from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import ImageJ
from .forms import ImageJForm
import cv2
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image

# Create your views here.
def index(request):
  images = ImageJ.objects.all()
  context = {
    'title': 'Index',
    'images': images
  }
  return render(request, 'imagej/index.html', context)

def show(request, id=None):
  instance = get_object_or_404(ImageJ, id=id)
  img = Image.open(instance.image.path).convert('L')

  WIDTH, HEIGHT = img.size

  data = np.asarray(img.getdata())
  data = data.reshape((HEIGHT, WIDTH))
  reduced_data = data.mean(axis=0)
  chart_data = [[i, int(x)] for i, x in enumerate(reduced_data)]

  context = {
    'title': 'Detail',
    'instance': instance,
    'data': chart_data
  }
  return render(request, 'imagej/show.html', context)

def create(request):
  form = ImageJForm(request.POST, request.FILES or None)
  if form.is_valid():
    instance = form.save(commit=False)
    instance.save()
  context = {
    'title': 'Create',
    'form': form
  }
  return render(request, 'imagej/form.html', context)



