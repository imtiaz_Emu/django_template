from django.db import models
import os
from django.conf import settings


def upload_location(instance, filename):
  return "%s/%s/%s" %('imagej', instance.id, filename)


# Create your models here.
class ImageJ(models.Model):
  title = models.CharField(max_length=120)
  image = models.ImageField(upload_to=upload_location,
                            height_field="height_field",
                            width_field="width_field",
                            null=False)
  height_field = models.IntegerField(default=0)
  width_field = models.IntegerField(default=0)
