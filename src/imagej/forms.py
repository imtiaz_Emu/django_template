from django import forms

from .models import ImageJ

class ImageJForm(forms.ModelForm):
  class Meta:
    model = ImageJ
    fields = [
      'title',
      'image',
    ]