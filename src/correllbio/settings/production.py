from .base import *

ENVIRONMENT = 'production'
DEBUG = False
ALLOWED_HOSTS = ['']
DATABASES = {}
DATABASES['default'] = dj_database_url.config(conn_max_age=600)